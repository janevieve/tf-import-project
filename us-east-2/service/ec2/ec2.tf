
locals {
  project-name          = "EUREKA"
  instance_type         = "t2.small"
  ami_type              = "ami-096fda3c22c1c990a"
  instance_count_per_az = 2
  volume_size           = 10
  custom_tags = {
    "CreatedBy"    = "TERRAFORM"
    "BusinessUnit" = "AVS",
    "CostCenter"   = "000000"
    "Environment"  = "DEV"
  }

}
data "aws_vpc" "vpc" {
  tags = {
    Name = "${local.project-name}-VPC"
  }

}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.vpc.id
}



module "security_group" {
  source      = "terraform-aws-modules/security-group/aws"
  version     = "~> 3.0"
  name        = "${local.project-name}-SG"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.vpc.id
  ingress_with_cidr_blocks = [
    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      description = "VPC CIDR"
      cidr_blocks = "10.20.30.0/24"
    },
    {
      from_port   = 22
      to_port     = 22
      protocol    = "-1"
      description = "SSH ports"
      cidr_blocks = "10.20.30.0/24"
    },
  ]

  egress_rules = ["all-all"]
}

resource "tls_private_key" "this" {
  algorithm = "RSA"
}

module "key_pair" {
  source     = "terraform-aws-modules/key-pair/aws"
  key_name   = "${local.project-name}-key"
  public_key = tls_private_key.this.public_key_openssh
}

module "ec2_instance-subnet1" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = "${local.project-name}-SUB1"
  instance_count         = local.instance_count_per_az
  ami                    = data.aws_ami.ami_type.id
  instance_type          = local.instance_type
  key_name               = module.key_pair.this_key_pair_key_name
  monitoring             = true
  vpc_security_group_ids = [module.security_group.this_security_group_id]
  subnet_id              = tolist(data.aws_subnet_ids.all.ids)[0]
  tags                   = local.custom_tags
}

resource "aws_volume_attachment" "this_ec2_sub1" {
  count       = local.instance_count_per_az
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.this_sub1[count.index].id
  instance_id = module.ec2_instance-subnet1.id[count.index]
}

resource "aws_ebs_volume" "this_sub1" {
  count             = local.instance_count_per_az
  availability_zone = module.ec2_instance-subnet1.availability_zone[count.index]
  size              = local.volume_size
  tags = {
    Name = "${local.project-name}-SUB1-VOLUME${count.index + 1}"
  }
}
module "ec2_instance-subnet2" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"
  name                   = "${local.project-name}-SUB2"
  instance_count         = local.instance_count_per_az
  ami                    = data.aws_ami.ami_type.id
  instance_type          = local.instance_type
  key_name               = module.key_pair.this_key_pair_key_name
  monitoring             = true
  vpc_security_group_ids = [module.security_group.this_security_group_id]
  subnet_id              = tolist(data.aws_subnet_ids.all.ids)[1]
  tags                   = local.custom_tags
}
resource "aws_volume_attachment" "this_ec2_sub2" {
  count       = local.instance_count_per_az
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.this_sub2[count.index].id
  instance_id = module.ec2_instance-subnet2.id[count.index]
}

resource "aws_ebs_volume" "this_sub2" {
  count             = local.instance_count_per_az
  availability_zone = module.ec2_instance-subnet2.availability_zone[count.index]
  size              = local.volume_size
  tags = {
    Name = "${local.project-name}-SUB2-VOLUME${count.index + 1}"
  }
}
