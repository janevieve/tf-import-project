output "sub1_instances_public_ips" {
  description = "Public IPs assigned to the EC2 instance"
  value       = module.ec2_instance-subnet1.public_ip
}

output "sub2_instances_public_ips" {
  description = "Public IPs assigned to the EC2 instance"
  value       = module.ec2_instance-subnet2.public_ip
}
